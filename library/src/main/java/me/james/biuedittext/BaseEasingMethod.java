package me.james.biuedittext;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class BaseEasingMethod {
    protected float mDuration;
    private ArrayList<EasingListener> mListeners = new ArrayList();

    /**
     * 构造方式
     *
     * @param duration
     */
    public BaseEasingMethod(float duration) {
        this.mDuration = duration;
    }

    /**
     * 添加EasingListener监听
     *
     * @param l
     */
    public void addEasingListener(BaseEasingMethod.EasingListener l) {
        this.mListeners.add(l);
    }

    /**
     * 添加EasingListener监听
     *
     * @param ls
     */
    public void addEasingListeners(BaseEasingMethod.EasingListener... ls) {
        BaseEasingMethod.EasingListener[] arr = ls;
        int mLen = ls.length;

        for (int i = 0; i < mLen; ++i) {
            BaseEasingMethod.EasingListener l = arr[i];
            this.mListeners.add(l);
        }
    }

    /**
     * removeEasingListener
     *
     * @param l 接口
     */
    public void removeEasingListener(BaseEasingMethod.EasingListener l) {
        this.mListeners.remove(l);
    }

    /**
     * 清除监听
     */
    public void clearEasingListeners() {
        this.mListeners.clear();
    }

    public void setDuration(float duration) {
        this.mDuration = duration;
    }

    /**
     * 执行on方法
     *
     * @param fraction float
     * @param startValue Number
     * @param endValue Number
     * @return Float
     */
    public final Float evaluate(float fraction, Number startValue, Number endValue) {
        float one = this.mDuration * fraction;
        float two = startValue.floatValue();
        float three = endValue.floatValue() - startValue.floatValue();
        float four = this.mDuration;
        float result = this.calculate(one, two, three, four).floatValue();
        Iterator iterator = this.mListeners.iterator();

        while (iterator.hasNext()) {
            BaseEasingMethod.EasingListener l = (BaseEasingMethod.EasingListener)iterator.next();
            l.on(one, result, two, three, four);
        }
        return Float.valueOf(result);
    }

    /**
     * calculate方法
     *
     * @param var1 float
     * @param var2 float
     * @param var3 float
     * @param var4 float
     * @return Float
     */
    public abstract Float calculate(float var1, float var2, float var3, float var4);

    public interface EasingListener {
        /**
         * on方法
         *
         * @param var1 float
         * @param var2 float
         * @param var3 float
         * @param var4 float
         * @param var5 float
         */
        void on(float var1, float var2, float var3, float var4, float var5);
    }
}