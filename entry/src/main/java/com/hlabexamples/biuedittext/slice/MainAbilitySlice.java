/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hlabexamples.biuedittext.slice;

import com.hlabexamples.biuedittext.ResourceTable;

import me.james.biuedittext.BiuEditText;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Button button = (Button) findComponentById(ResourceTable.Id_clear);
        StackLayout directionalLayout = (StackLayout) findComponentById(ResourceTable.Id_layout);
        BiuEditText biuEditText1 = (BiuEditText) findComponentById(ResourceTable.Id_biu1);
        BiuEditText biuEditText2 = (BiuEditText) findComponentById(ResourceTable.Id_biu2);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                biuEditText1.setPosition(biuEditText1.getText().length() - 1);
                biuEditText1.setText("");
                biuEditText2.setPosition(biuEditText2.getText().length() - 1);
                biuEditText2.setText("");
            }
        });
        biuEditText1.setContentContainer(directionalLayout);
        biuEditText2.setContentContainer(directionalLayout);
    }
}