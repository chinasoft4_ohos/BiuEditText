# BiuEditText

#### 项目介绍

* 项目名称：BiuEditText
* 所属系列：openharmony的第三方组件适配移植
* 功能：输入或删除字符时会有弹出效果
* 项目移植状态：主功能完成
* 调用差异：根布局需要相对布局，且需要设置布局
* 开发版本：sdk6，DevEco Studio2.2 Beta1
* 基线版本：Relesase 1.4.1

#### 效果演示

![](https://images.gitee.com/uploads/images/2021/0616/172353_d4fdeee0_7648707.gif "BiuEditText.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

```txt
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```txt
dependencies {
    implementation('com.gitee.chinasoft_ohos:BiuEditText:1.0.1')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在xml布局中引用

```txt
<me.james.biuedittext.BiuEditText
        ohos:below="$id:clear"
        ohos:id="$+id:biu1"
        ohos:text_size="15fp"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:top_margin="100vp"
        ohos:hint="biu,biu,发射1号"
        ohos:text_color="#FFF"
        ohos:basement="#00475E"
        app:biu_duration="800"
        app:biu_text_color="#FFF"
        app:biu_text_scale="1.5"
        app:biu_text_start_size="30fp"
        app:biu_type="flydown"/>
```

根布局需要设置为相对布局

```txt
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res/ohos"
    ohos:padding="16vp"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:id="$+id:layout"
    ohos:background_element="#0099CC"
    ohos:orientation="vertical">
```

并在外部设置布局

```txt
DependentLayout directionalLayout = (DependentLayout) findComponentById(ResourceTable.Id_layout);
        BiuEditText biuEditText1 = (BiuEditText)findComponentById(ResourceTable.Id_biu2);
biuEditText1.setContentContainer(directionalLayout);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

1.0.1

#### 版权和许可信息

BiuEditText is opensource, contribution and feedback are welcomed

[Apache Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

```txt
Copyright 2015 Supercharge

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

